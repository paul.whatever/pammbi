# Downloading p-boot-conf and p-boot-unconf in order to adapt the boot-export directory
cd ~/Downloads
wget --no-check-certificate https://megous.com/git/p-boot/plain/dist/p-boot-conf
wget --no-check-certificate https://megous.com/git/p-boot/plain/dist/p-boot-unconf
chmod +x p-boot-conf
chmod +x p-boot-unconf
mv p-boot-conf /bin
mv p-boot-unconf /bin
p-boot-unconf boot-export /dev/mmcblk2p1

# Update the Kernel
wget --no-check-certificate https://xff.cz/kernels/5.16/ppd.tar.gz
gunzip ppd.tar.gz
tar -xvf ppd.tar
cd ppd-5.16/
mv Image linux-0.img
mv board-1.1.dtb dtb-0.img
mv board-1.2.dtb dtb2-0.img
cp linux-0.img dtb-0.img dtb2.img ~/Downloads/boot-export
cd ..
p-boot-conf boot-export /dev/mmcblk2p1